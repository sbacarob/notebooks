class AddNotebookIdToEntries < ActiveRecord::Migration
  def change
    add_column :entries, :notebook_id, :integer
  end
end
