class AddFontToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :font, :string
  end
end
