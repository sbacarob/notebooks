class AddTfontToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :tfont, :string
  end
end
