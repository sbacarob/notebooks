class CreateNotebooks < ActiveRecord::Migration
  def change
    create_table :notebooks do |t|
      t.integer :user_id
      t.text :name
      t.text :subject
      t.date :created
      t.text :cover

      t.timestamps
    end
  end
end
