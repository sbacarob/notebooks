class AddPaperToNotebook < ActiveRecord::Migration
  def change
    add_column :notebooks, :paper, :string
  end
end
