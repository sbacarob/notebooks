json.array!(@entries) do |entry|
  json.extract! entry, :id, :topic_id, :name, :content, :created
  json.url entry_url(entry, format: :json)
end
