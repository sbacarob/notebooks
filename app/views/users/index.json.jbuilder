json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :password, :lname
  json.url user_url(user, format: :json)
end
