/**
 * Método que cambia las imágenes de los cuadernos cuando el usuario hace click en la portada del cuaderno
 */
$('.img_pag_2').click(function(){
    $('.img_pag_1').attr('src','/assets/notebook_page_1.jpg');
    $('.img_pag_2').attr('src','/assets/notebook_page.jpg')
    cambiart();
});

/**
 * Método que modifica la altura de la página izquierda del cuaderno de acuerdo a la altura de la imagen de la derecha
 */
function cambiart(){
    setTimeout(function(){
        var altura= $('#pg_1_container').css('height');
        console.log(altura.substring(0,altura.length-2));
        var altdef= parseInt(altura.substring(0,altura.length-2));
        $('#pg_1_content').css('height',altdef-100 + "px");

    },500);
}

/**
 * Variable para guardar los topics que hay creados hasta el momento
 * @type {Array}
 */
var tags=[];

/**
 * Variable para guardar los ids de los topics de la anterior lista
 * @type {Array}
 */
var tids=[];

/**
 * Variable para indicar el indice actual de alguna cosa que no me acuerdo
 */
var cind;

/**
 * Variable que guarda el valor del último indice creado
 * @type {Number}
 */
var ltid= parseInt($("#last-topic-id").text());

/**
 * Método que carga los tags del html a la variable
 */
$("#n_topics").find("li").each(function(){
    tags.push($(this).text());
});

/**
 * Método que carga los ids de los tags del html a la variable
 */
$("#n_topics_ids").find("li").each(function(){
    tids.push($(this).text());
});

/**
 * Método de jQueryUI para dar al textbox la función de autocomplete
 */
$( "#topic_display" ).autocomplete({
    source: function( request, response ) {
        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( tags, function( item ){
            return matcher.test( item );
        }) );
    },
    select: function(event,ui){
        $("#entry_topic_id").val(ui.item.id+1);
    }
});

/**
 * Método que selecciona como índice actual el del tag que el usuario seleccionó del autocomplete o 0 si no existía el tag
 */
$("#topic_display").focusout(function(){
    if($(this).val()!=""){
        indice=tags.indexOf($(this).val())+1;
        if(indice!=0) {
            cind = tids[indice - 1];
        }
        else{
            cind=0;
        }
    }
});

/**
 * Método que hace un pedido ajax para crear un topic en caso de que el usuario haya escogido un nombre de topic que no
 * existía.
 */
$("#but-sbmt-frm").click(function(){
    if(cind!=0) {
        $("#topic_id").val(cind);
    }
    else{
        $("#topic_name").val($("#topic_display").val());
        var vts= $("#new_topic").serialize();
        $.ajax({
            type:"POST",
            url:"/topics",
            data:vts,
            dataType:"JSON"
        }).success(function(json){
            console.log("success",json);
        })
        $("#topic_id").val(ltid+1);
        ltid+=1;
    }
});