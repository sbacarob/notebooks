class Topic < ActiveRecord::Base
  belongs_to :notebook
  has_many :entries, dependent: :destroy
end
