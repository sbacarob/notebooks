class NotebooksController < ApplicationController
  before_action :set_notebook, only: [:show,:edit,:delete]


  def create
    @notebook = current_user.notebooks.new(notebook_params)

    respond_to do |format|
      if @notebook.save
        format.html { redirect_to @notebook, notice: 'Notebook was successfully created.' }
        format.json { render :show, status: :created, location: @notebook }
      else
        format.html { render :new }
        format.json { render json: @notebook.errors, status: :unprocessable_entity }
      end
    end
  end

  def editor
  end

  def show
  end

  def edit
  end

  def delete
  end

  private
  def set_notebook
    @notebook = Notebook.find(params[:id])
  end

  def notebook_params
    params.require(:notebook).permit(:name, :subject, :cover, :paper, :tfont, :font)
  end
end
